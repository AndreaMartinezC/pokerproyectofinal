﻿namespace Poker
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.BtnDeal = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btndescartar = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.btnlisto = new System.Windows.Forms.Button();
            this.palomita1 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.palomita2 = new System.Windows.Forms.PictureBox();
            this.palomita3 = new System.Windows.Forms.PictureBox();
            this.palomita4 = new System.Windows.Forms.PictureBox();
            this.palomita5 = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.mostrartodo = new System.Windows.Forms.Button();
            this.lblmanop4 = new System.Windows.Forms.Label();
            this.lblmanop3 = new System.Windows.Forms.Label();
            this.lblmanop2 = new System.Windows.Forms.Label();
            this.lblmanop1 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblganador = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.palomita1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.palomita2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.palomita3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.palomita4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.palomita5)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(770, 638);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(258, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "PLAYER 1 (DEALER)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(1323, 453);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 29);
            this.label2.TabIndex = 1;
            this.label2.Text = "PLAYER 2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(839, 290);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(133, 29);
            this.label3.TabIndex = 2;
            this.label3.Text = "PLAYER 3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(301, 471);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(133, 29);
            this.label4.TabIndex = 3;
            this.label4.Text = "PLAYER 4";
            // 
            // BtnDeal
            // 
            this.BtnDeal.Location = new System.Drawing.Point(827, 443);
            this.BtnDeal.Name = "BtnDeal";
            this.BtnDeal.Size = new System.Drawing.Size(131, 57);
            this.BtnDeal.TabIndex = 24;
            this.BtnDeal.Text = "DEAL";
            this.BtnDeal.UseVisualStyleBackColor = true;
            this.BtnDeal.Click += new System.EventHandler(this.BtnDeal_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(775, 594);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(237, 41);
            this.button1.TabIndex = 25;
            this.button1.Text = "Ver mis cartas";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(447, 670);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 17);
            this.label5.TabIndex = 26;
            this.label5.Text = "label5";
            this.label5.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(625, 670);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 17);
            this.label6.TabIndex = 27;
            this.label6.Text = "label6";
            this.label6.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(803, 670);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 17);
            this.label7.TabIndex = 28;
            this.label7.Text = "label7";
            this.label7.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(981, 670);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 17);
            this.label8.TabIndex = 29;
            this.label8.Text = "label8";
            this.label8.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(1159, 670);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 17);
            this.label9.TabIndex = 30;
            this.label9.Text = "label9";
            this.label9.Visible = false;
            // 
            // btndescartar
            // 
            this.btndescartar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btndescartar.Location = new System.Drawing.Point(827, 523);
            this.btndescartar.Name = "btndescartar";
            this.btndescartar.Size = new System.Drawing.Size(139, 37);
            this.btndescartar.TabIndex = 31;
            this.btndescartar.Text = "Descartar";
            this.btndescartar.UseVisualStyleBackColor = true;
            this.btndescartar.Visible = false;
            this.btndescartar.Click += new System.EventHandler(this.btndescartar_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(734, 505);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(339, 29);
            this.label10.TabIndex = 33;
            this.label10.Text = "Seleccione maximo 3 cartas";
            this.label10.Visible = false;
            // 
            // btnlisto
            // 
            this.btnlisto.Location = new System.Drawing.Point(858, 566);
            this.btnlisto.Name = "btnlisto";
            this.btnlisto.Size = new System.Drawing.Size(75, 23);
            this.btnlisto.TabIndex = 34;
            this.btnlisto.Text = "Listo";
            this.btnlisto.UseVisualStyleBackColor = true;
            this.btnlisto.Visible = false;
            this.btnlisto.Click += new System.EventHandler(this.btnlisto_Click);
            // 
            // palomita1
            // 
            this.palomita1.Image = global::Poker.Properties.Resources.Green_check_mark_symbol_round_sticker_rd595add23fee473ca44c12c0a1bdcd36_v9waf_8byvr_512;
            this.palomita1.Location = new System.Drawing.Point(522, 859);
            this.palomita1.Name = "palomita1";
            this.palomita1.Size = new System.Drawing.Size(100, 82);
            this.palomita1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.palomita1.TabIndex = 35;
            this.palomita1.TabStop = false;
            this.palomita1.Visible = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.Image = global::Poker.Properties.Resources.card_back_orange2;
            this.pictureBox15.Location = new System.Drawing.Point(12, 32);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(251, 172);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox15.TabIndex = 18;
            this.pictureBox15.TabStop = false;
            this.pictureBox15.Visible = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = global::Poker.Properties.Resources.card_back_orange2;
            this.pictureBox11.Location = new System.Drawing.Point(12, 210);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(251, 172);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox11.TabIndex = 14;
            this.pictureBox11.TabStop = false;
            this.pictureBox11.Visible = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = global::Poker.Properties.Resources.card_back_orange2;
            this.pictureBox12.Location = new System.Drawing.Point(12, 388);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(251, 172);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox12.TabIndex = 15;
            this.pictureBox12.TabStop = false;
            this.pictureBox12.Visible = false;
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = global::Poker.Properties.Resources.card_back_orange2;
            this.pictureBox13.Location = new System.Drawing.Point(12, 566);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(251, 172);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox13.TabIndex = 16;
            this.pictureBox13.TabStop = false;
            this.pictureBox13.Visible = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.Image = global::Poker.Properties.Resources.card_back_orange2;
            this.pictureBox14.Location = new System.Drawing.Point(12, 741);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(251, 172);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox14.TabIndex = 17;
            this.pictureBox14.TabStop = false;
            this.pictureBox14.Visible = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.Image = global::Poker.Properties.Resources.card_back_orange2;
            this.pictureBox16.Location = new System.Drawing.Point(1519, 741);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(251, 172);
            this.pictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox16.TabIndex = 19;
            this.pictureBox16.TabStop = false;
            this.pictureBox16.Visible = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.Image = global::Poker.Properties.Resources.card_back_orange2;
            this.pictureBox17.Location = new System.Drawing.Point(1519, 566);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(251, 172);
            this.pictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox17.TabIndex = 20;
            this.pictureBox17.TabStop = false;
            this.pictureBox17.Visible = false;
            // 
            // pictureBox18
            // 
            this.pictureBox18.Image = global::Poker.Properties.Resources.card_back_orange2;
            this.pictureBox18.Location = new System.Drawing.Point(1519, 388);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(251, 172);
            this.pictureBox18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox18.TabIndex = 21;
            this.pictureBox18.TabStop = false;
            this.pictureBox18.Visible = false;
            // 
            // pictureBox19
            // 
            this.pictureBox19.Image = global::Poker.Properties.Resources.card_back_orange2;
            this.pictureBox19.Location = new System.Drawing.Point(1519, 210);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(251, 172);
            this.pictureBox19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox19.TabIndex = 22;
            this.pictureBox19.TabStop = false;
            this.pictureBox19.Visible = false;
            // 
            // pictureBox20
            // 
            this.pictureBox20.Image = global::Poker.Properties.Resources.card_back_orange2;
            this.pictureBox20.Location = new System.Drawing.Point(1519, 32);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(251, 172);
            this.pictureBox20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox20.TabIndex = 23;
            this.pictureBox20.TabStop = false;
            this.pictureBox20.Visible = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = global::Poker.Properties.Resources.card_back_orange;
            this.pictureBox10.Location = new System.Drawing.Point(1162, 12);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(172, 251);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox10.TabIndex = 13;
            this.pictureBox10.TabStop = false;
            this.pictureBox10.Visible = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::Poker.Properties.Resources.card_back_orange;
            this.pictureBox9.Location = new System.Drawing.Point(984, 12);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(172, 251);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 12;
            this.pictureBox9.TabStop = false;
            this.pictureBox9.Visible = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::Poker.Properties.Resources.card_back_orange;
            this.pictureBox8.Location = new System.Drawing.Point(806, 10);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(172, 251);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 11;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.Visible = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::Poker.Properties.Resources.card_back_orange;
            this.pictureBox7.Location = new System.Drawing.Point(628, 10);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(172, 251);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 10;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.Visible = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::Poker.Properties.Resources.card_back_orange;
            this.pictureBox6.Location = new System.Drawing.Point(450, 12);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(172, 251);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 9;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.Visible = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::Poker.Properties.Resources.card_back_orange;
            this.pictureBox5.Location = new System.Drawing.Point(1162, 690);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(172, 251);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 8;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Visible = false;
            this.pictureBox5.Click += new System.EventHandler(this.pictureBox5_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Poker.Properties.Resources.card_back_orange;
            this.pictureBox4.Location = new System.Drawing.Point(984, 690);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(172, 251);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 7;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Visible = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Poker.Properties.Resources.card_back_orange;
            this.pictureBox3.Location = new System.Drawing.Point(806, 690);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(172, 251);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 6;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Visible = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Poker.Properties.Resources.card_back_orange;
            this.pictureBox2.Location = new System.Drawing.Point(628, 690);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(172, 251);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Visible = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Poker.Properties.Resources.card_back_orange;
            this.pictureBox1.Location = new System.Drawing.Point(450, 690);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(172, 251);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // palomita2
            // 
            this.palomita2.Image = global::Poker.Properties.Resources.Green_check_mark_symbol_round_sticker_rd595add23fee473ca44c12c0a1bdcd36_v9waf_8byvr_512;
            this.palomita2.Location = new System.Drawing.Point(700, 859);
            this.palomita2.Name = "palomita2";
            this.palomita2.Size = new System.Drawing.Size(100, 82);
            this.palomita2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.palomita2.TabIndex = 36;
            this.palomita2.TabStop = false;
            this.palomita2.Visible = false;
            // 
            // palomita3
            // 
            this.palomita3.Image = global::Poker.Properties.Resources.Green_check_mark_symbol_round_sticker_rd595add23fee473ca44c12c0a1bdcd36_v9waf_8byvr_512;
            this.palomita3.Location = new System.Drawing.Point(878, 859);
            this.palomita3.Name = "palomita3";
            this.palomita3.Size = new System.Drawing.Size(100, 82);
            this.palomita3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.palomita3.TabIndex = 37;
            this.palomita3.TabStop = false;
            this.palomita3.Visible = false;
            // 
            // palomita4
            // 
            this.palomita4.Image = global::Poker.Properties.Resources.Green_check_mark_symbol_round_sticker_rd595add23fee473ca44c12c0a1bdcd36_v9waf_8byvr_512;
            this.palomita4.Location = new System.Drawing.Point(1056, 859);
            this.palomita4.Name = "palomita4";
            this.palomita4.Size = new System.Drawing.Size(100, 82);
            this.palomita4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.palomita4.TabIndex = 38;
            this.palomita4.TabStop = false;
            this.palomita4.Visible = false;
            // 
            // palomita5
            // 
            this.palomita5.Image = global::Poker.Properties.Resources.Green_check_mark_symbol_round_sticker_rd595add23fee473ca44c12c0a1bdcd36_v9waf_8byvr_512;
            this.palomita5.Location = new System.Drawing.Point(1234, 859);
            this.palomita5.Name = "palomita5";
            this.palomita5.Size = new System.Drawing.Size(100, 82);
            this.palomita5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.palomita5.TabIndex = 39;
            this.palomita5.TabStop = false;
            this.palomita5.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.LightGray;
            this.label11.Location = new System.Drawing.Point(570, 482);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 17);
            this.label11.TabIndex = 40;
            this.label11.Text = "label11";
            this.label11.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(783, 569);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(218, 17);
            this.label12.TabIndex = 41;
            this.label12.Text = "Solo pueden ser 3 cartas maximo";
            this.label12.Visible = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(858, 595);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 42;
            this.button2.Text = "Ok";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // mostrartodo
            // 
            this.mostrartodo.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mostrartodo.Location = new System.Drawing.Point(658, 367);
            this.mostrartodo.Name = "mostrartodo";
            this.mostrartodo.Size = new System.Drawing.Size(483, 70);
            this.mostrartodo.TabIndex = 43;
            this.mostrartodo.Text = "Mostrar manos de todos los jugadores";
            this.mostrartodo.UseVisualStyleBackColor = true;
            this.mostrartodo.Visible = false;
            this.mostrartodo.Click += new System.EventHandler(this.mostrartodo_Click);
            // 
            // lblmanop4
            // 
            this.lblmanop4.AutoSize = true;
            this.lblmanop4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmanop4.ForeColor = System.Drawing.Color.Yellow;
            this.lblmanop4.Location = new System.Drawing.Point(330, 500);
            this.lblmanop4.Name = "lblmanop4";
            this.lblmanop4.Size = new System.Drawing.Size(75, 25);
            this.lblmanop4.TabIndex = 44;
            this.lblmanop4.Text = "label13";
            this.lblmanop4.Visible = false;
            // 
            // lblmanop3
            // 
            this.lblmanop3.AutoSize = true;
            this.lblmanop3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmanop3.ForeColor = System.Drawing.Color.Yellow;
            this.lblmanop3.Location = new System.Drawing.Point(875, 319);
            this.lblmanop3.Name = "lblmanop3";
            this.lblmanop3.Size = new System.Drawing.Size(75, 25);
            this.lblmanop3.TabIndex = 45;
            this.lblmanop3.Text = "label14";
            this.lblmanop3.Visible = false;
            // 
            // lblmanop2
            // 
            this.lblmanop2.AutoSize = true;
            this.lblmanop2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmanop2.ForeColor = System.Drawing.Color.Yellow;
            this.lblmanop2.Location = new System.Drawing.Point(1339, 482);
            this.lblmanop2.Name = "lblmanop2";
            this.lblmanop2.Size = new System.Drawing.Size(75, 25);
            this.lblmanop2.TabIndex = 46;
            this.lblmanop2.Text = "label15";
            this.lblmanop2.Visible = false;
            // 
            // lblmanop1
            // 
            this.lblmanop1.AutoSize = true;
            this.lblmanop1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmanop1.ForeColor = System.Drawing.Color.Yellow;
            this.lblmanop1.Location = new System.Drawing.Point(865, 609);
            this.lblmanop1.Name = "lblmanop1";
            this.lblmanop1.Size = new System.Drawing.Size(75, 25);
            this.lblmanop1.TabIndex = 47;
            this.lblmanop1.Text = "label16";
            this.lblmanop1.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.label13.Location = new System.Drawing.Point(515, 440);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(305, 38);
            this.label13.TabIndex = 48;
            this.label13.Text = "EL GANADOR ES: ";
            this.label13.Visible = false;
            // 
            // lblganador
            // 
            this.lblganador.AutoSize = true;
            this.lblganador.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblganador.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.lblganador.Location = new System.Drawing.Point(820, 440);
            this.lblganador.Name = "lblganador";
            this.lblganador.Size = new System.Drawing.Size(143, 38);
            this.lblganador.TabIndex = 49;
            this.lblganador.Text = "ganador";
            this.lblganador.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkGreen;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(1782, 1055);
            this.Controls.Add(this.lblganador);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.lblmanop1);
            this.Controls.Add(this.lblmanop2);
            this.Controls.Add(this.lblmanop3);
            this.Controls.Add(this.lblmanop4);
            this.Controls.Add(this.mostrartodo);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.palomita5);
            this.Controls.Add(this.palomita4);
            this.Controls.Add(this.palomita3);
            this.Controls.Add(this.palomita2);
            this.Controls.Add(this.palomita1);
            this.Controls.Add(this.btnlisto);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.btndescartar);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pictureBox15);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.pictureBox12);
            this.Controls.Add(this.pictureBox13);
            this.Controls.Add(this.pictureBox14);
            this.Controls.Add(this.pictureBox16);
            this.Controls.Add(this.pictureBox17);
            this.Controls.Add(this.pictureBox18);
            this.Controls.Add(this.pictureBox19);
            this.Controls.Add(this.pictureBox20);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.BtnDeal);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Poker";
            ((System.ComponentModel.ISupportInitialize)(this.palomita1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.palomita2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.palomita3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.palomita4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.palomita5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.Button BtnDeal;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btndescartar;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnlisto;
        private System.Windows.Forms.PictureBox palomita1;
        private System.Windows.Forms.PictureBox palomita2;
        private System.Windows.Forms.PictureBox palomita3;
        private System.Windows.Forms.PictureBox palomita4;
        private System.Windows.Forms.PictureBox palomita5;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button mostrartodo;
        private System.Windows.Forms.Label lblmanop4;
        private System.Windows.Forms.Label lblmanop3;
        private System.Windows.Forms.Label lblmanop2;
        private System.Windows.Forms.Label lblmanop1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblganador;
    }
}

