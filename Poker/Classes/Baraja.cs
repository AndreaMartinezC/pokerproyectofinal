﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker.Classes
{
    class Baraja:Carta 
    {
        //const int CantidadCartas = 52;
        private Carta[] baraja;
        private Carta[] m1;
        private Carta[] desca;
        
        

        public Baraja()
        {
            baraja = new Carta[52];
            desca = new Carta[5];
            m1 = new Carta[5];
        }

        public Carta[] obtenerbaraja { get { return baraja; } }

        //crear baraja de 52 cartas, 13 valores cada uno con 4 palos

        public void SetBaraja()
        {
            int i = 0;
            foreach(PALO p in Enum.GetValues(typeof(PALO)))
            {
                foreach(VALOR v in Enum.GetValues(typeof(VALOR)))
                {
                    baraja[i] = new Carta { palo = p, valor = v };
                    i++;

                }
            }

            barajear();
        }

        public void barajear()
        {
            Random rand = new Random();
            Carta temporal;

            for (int veces = 0; veces<500; veces++)
            {
                for (int i=0; i<52; i++)
                {
                    int Carta2Index = rand.Next(13); //xk hay 13 cartas de 1 palo
                    temporal = baraja[i];
                    baraja[i] = baraja[Carta2Index];
                    baraja[Carta2Index] = temporal; 
                }
            }
        }

        public Carta[] OrdenarMano(Carta[] m1)
        {
            for (int i = 0; i < m1.Length - 1; i++)
            {
                int min = i;
                for (int j = i + 1; j < m1.Length; j++)
                    if ((int)m1[j].valor < (int)m1[min].valor) min = j;
                Carta temp = m1[i];
                m1[i] = m1[min];
                //int ki = (int)manoP1[min].valor;
                m1[min] = temp;

            }

            return m1 ;
        }

        public int RevisarManoC1(Carta[] m1)
        {
            int c1=0;
                

            if(m1[0].valor==Carta.VALOR.As && m1[0].palo==Carta.PALO.Corazon)
            {
                c1 = 1;
            }
            else if (m1[0].valor == Carta.VALOR.Rey && m1[0].palo == Carta.PALO.Corazon)
            {
                c1 = 2;
            }
            else if (m1[0].valor == Carta.VALOR.Reina && m1[0].palo == Carta.PALO.Corazon)
            {
                c1 = 3;
            }
            else if (m1[0].valor == Carta.VALOR.Joker && m1[0].palo == Carta.PALO.Corazon)
            {
                c1 = 4;
            }
            else if (m1[0].valor == Carta.VALOR.Diez && m1[0].palo == Carta.PALO.Corazon)
            {
                c1 = 5;
            }
            else if (m1[0].valor == Carta.VALOR.Nueve && m1[0].palo == Carta.PALO.Corazon)
            {
                c1 = 6;
            }
            else if (m1[0].valor == Carta.VALOR.Ocho && m1[0].palo == Carta.PALO.Corazon)
            {
                c1 = 7;
            }
            else if (m1[0].valor == Carta.VALOR.Siete && m1[0].palo == Carta.PALO.Corazon)
            {
                c1 = 8;
            }
            else if (m1[0].valor == Carta.VALOR.Seis && m1[0].palo == Carta.PALO.Corazon)
            {
                c1 = 9;
            }
            else if (m1[0].valor == Carta.VALOR.Cinco && m1[0].palo == Carta.PALO.Corazon)
            {
                c1 = 10;
            }
            else if (m1[0].valor == Carta.VALOR.Cuatro && m1[0].palo == Carta.PALO.Corazon)
            {
                c1 = 11;
            }
            else if (m1[0].valor == Carta.VALOR.Tres && m1[0].palo == Carta.PALO.Corazon)
            {
                c1 = 12;
            }
            else if (m1[0].valor == Carta.VALOR.Dos && m1[0].palo == Carta.PALO.Corazon)
            {
                c1 = 13;
            }

            else if (m1[0].valor == Carta.VALOR.As && m1[0].palo == Carta.PALO.Espada)
            {
                c1 = 14;
            }
            else if (m1[0].valor == Carta.VALOR.Rey && m1[0].palo == Carta.PALO.Espada)
            {
                c1 = 15;
            }
            else if (m1[0].valor == Carta.VALOR.Reina && m1[0].palo == Carta.PALO.Espada)
            {
                c1 = 16;
            }
            else if (m1[0].valor == Carta.VALOR.Joker && m1[0].palo == Carta.PALO.Espada)
            {
                c1 = 17;
            }
            else if (m1[0].valor == Carta.VALOR.Diez && m1[0].palo == Carta.PALO.Espada)
            {
                c1 = 18;
            }
            else if (m1[0].valor == Carta.VALOR.Nueve && m1[0].palo == Carta.PALO.Espada)
            {
                c1 = 19;
            }
            else if (m1[0].valor == Carta.VALOR.Ocho && m1[0].palo == Carta.PALO.Espada)
            {
                c1 = 20;
            }
            else if (m1[0].valor == Carta.VALOR.Siete && m1[0].palo == Carta.PALO.Espada)
            {
                c1 = 21;
            }
            else if (m1[0].valor == Carta.VALOR.Seis && m1[0].palo == Carta.PALO.Espada)
            {
                c1 = 22;
            }
            else if (m1[0].valor == Carta.VALOR.Cinco && m1[0].palo == Carta.PALO.Espada)
            {
                c1 = 23;
            }
            else if (m1[0].valor == Carta.VALOR.Cuatro && m1[0].palo == Carta.PALO.Espada)
            {
                c1 = 24;
            }
            else if (m1[0].valor == Carta.VALOR.Tres && m1[0].palo == Carta.PALO.Espada)
            {
                c1 = 25;
            }
            else if (m1[0].valor == Carta.VALOR.Dos && m1[0].palo == Carta.PALO.Espada)
            {
                c1 = 26;
            }

            else if (m1[0].valor == Carta.VALOR.As && m1[0].palo == Carta.PALO.Diamante)
            {
                c1 = 27;
            }
            else if (m1[0].valor == Carta.VALOR.Rey && m1[0].palo == Carta.PALO.Diamante)
            {
                c1 = 28;
            }
            else if (m1[0].valor == Carta.VALOR.Reina && m1[0].palo == Carta.PALO.Diamante)
            {
                c1 = 29;
            }
            else if (m1[0].valor == Carta.VALOR.Joker && m1[0].palo == Carta.PALO.Diamante)
            {
                c1 = 30;
            }
            else if (m1[0].valor == Carta.VALOR.Diez && m1[0].palo == Carta.PALO.Diamante)
            {
                c1 = 31;
            }
            else if (m1[0].valor == Carta.VALOR.Nueve && m1[0].palo == Carta.PALO.Diamante)
            {
                c1 = 32;
            }
            else if (m1[0].valor == Carta.VALOR.Ocho && m1[0].palo == Carta.PALO.Diamante)
            {
                c1 = 33;
            }
            else if (m1[0].valor == Carta.VALOR.Siete && m1[0].palo == Carta.PALO.Diamante)
            {
                c1 = 34;
            }
            else if (m1[0].valor == Carta.VALOR.Seis && m1[0].palo == Carta.PALO.Diamante)
            {
                c1 = 35;
            }
            else if (m1[0].valor == Carta.VALOR.Cinco && m1[0].palo == Carta.PALO.Diamante)
            {
                c1 = 36;
            }
            else if (m1[0].valor == Carta.VALOR.Cuatro && m1[0].palo == Carta.PALO.Diamante)
            {
                c1 = 37;
            }
            else if (m1[0].valor == Carta.VALOR.Tres && m1[0].palo == Carta.PALO.Diamante)
            {
                c1 = 38;
            }
            else if (m1[0].valor == Carta.VALOR.Dos && m1[0].palo == Carta.PALO.Diamante)
            {
                c1 = 39;
            }

            else if (m1[0].valor == Carta.VALOR.As && m1[0].palo == Carta.PALO.Trebol)
            {
                c1 = 40;
            }
            else if (m1[0].valor == Carta.VALOR.Rey && m1[0].palo == Carta.PALO.Trebol)
            {
                c1 = 41;
            }
            else if (m1[0].valor == Carta.VALOR.Reina && m1[0].palo == Carta.PALO.Trebol)
            {
                c1 = 42;
            }
            else if (m1[0].valor == Carta.VALOR.Joker && m1[0].palo == Carta.PALO.Trebol)
            {
                c1 = 43;
            }
            else if (m1[0].valor == Carta.VALOR.Diez && m1[0].palo == Carta.PALO.Trebol)
            {
                c1 = 44;
            }
            else if (m1[0].valor == Carta.VALOR.Nueve && m1[0].palo == Carta.PALO.Trebol)
            {
                c1 = 45;
            }
            else if (m1[0].valor == Carta.VALOR.Ocho && m1[0].palo == Carta.PALO.Trebol)
            {
                c1 = 46;
            }
            else if (m1[0].valor == Carta.VALOR.Siete && m1[0].palo == Carta.PALO.Trebol)
            {
                c1 = 47;
            }
            else if (m1[0].valor == Carta.VALOR.Seis && m1[0].palo == Carta.PALO.Trebol)
            {
                c1 = 48;
            }
            else if (m1[0].valor == Carta.VALOR.Cinco && m1[0].palo == Carta.PALO.Trebol)
            {
                c1 = 49;
            }
            else if (m1[0].valor == Carta.VALOR.Cuatro && m1[0].palo == Carta.PALO.Trebol)
            {
                c1 = 50;
            }
            else if (m1[0].valor == Carta.VALOR.Tres && m1[0].palo == Carta.PALO.Trebol)
            {
                c1 = 51;
            }
            else if (m1[0].valor == Carta.VALOR.Dos && m1[0].palo == Carta.PALO.Trebol)
            {
                c1 = 52;
            }

            return c1;

        }

        public int RevisarManoC2(Carta[] m1)
        {
            int c2 = 0;


            if (m1[1].valor == Carta.VALOR.As && m1[1].palo == Carta.PALO.Corazon)
            {
                c2 = 1;
            }
            else if (m1[1].valor == Carta.VALOR.Rey && m1[1].palo == Carta.PALO.Corazon)
            {
                c2 = 2;
            }
            else if (m1[1].valor == Carta.VALOR.Reina && m1[1].palo == Carta.PALO.Corazon)
            {
                c2 = 3;
            }
            else if (m1[1].valor == Carta.VALOR.Joker && m1[1].palo == Carta.PALO.Corazon)
            {
                c2 = 4;
            }
            else if (m1[1].valor == Carta.VALOR.Diez && m1[1].palo == Carta.PALO.Corazon)
            {
                c2 = 5;
            }
            else if (m1[1].valor == Carta.VALOR.Nueve && m1[1].palo == Carta.PALO.Corazon)
            {
                c2 = 6;
            }
            else if (m1[1].valor == Carta.VALOR.Ocho && m1[1].palo == Carta.PALO.Corazon)
            {
                c2 = 7;
            }
            else if (m1[1].valor == Carta.VALOR.Siete && m1[1].palo == Carta.PALO.Corazon)
            {
                c2 = 8;
            }
            else if (m1[1].valor == Carta.VALOR.Seis && m1[1].palo == Carta.PALO.Corazon)
            {
                c2 = 9;
            }
            else if (m1[1].valor == Carta.VALOR.Cinco && m1[1].palo == Carta.PALO.Corazon)
            {
                c2 = 10;
            }
            else if (m1[1].valor == Carta.VALOR.Cuatro && m1[1].palo == Carta.PALO.Corazon)
            {
                c2 = 11;
            }
            else if (m1[1].valor == Carta.VALOR.Tres && m1[1].palo == Carta.PALO.Corazon)
            {
                c2 = 12;
            }
            else if (m1[1].valor == Carta.VALOR.Dos && m1[1].palo == Carta.PALO.Corazon)
            {
                c2 = 13;
            }

            else if (m1[1].valor == Carta.VALOR.As && m1[1].palo == Carta.PALO.Espada)
            {
                c2 = 14;
            }
            else if (m1[1].valor == Carta.VALOR.Rey && m1[1].palo == Carta.PALO.Espada)
            {
                c2 = 15;
            }
            else if (m1[1].valor == Carta.VALOR.Reina && m1[1].palo == Carta.PALO.Espada)
            {
                c2 = 16;
            }
            else if (m1[1].valor == Carta.VALOR.Joker && m1[1].palo == Carta.PALO.Espada)
            {
                c2 = 17;
            }
            else if (m1[1].valor == Carta.VALOR.Diez && m1[1].palo == Carta.PALO.Espada)
            {
                c2 = 18;
            }
            else if (m1[1].valor == Carta.VALOR.Nueve && m1[1].palo == Carta.PALO.Espada)
            {
                c2 = 19;
            }
            else if (m1[1].valor == Carta.VALOR.Ocho && m1[1].palo == Carta.PALO.Espada)
            {
                c2 = 20;
            }
            else if (m1[1].valor == Carta.VALOR.Siete && m1[1].palo == Carta.PALO.Espada)
            {
                c2 = 21;
            }
            else if (m1[1].valor == Carta.VALOR.Seis && m1[1].palo == Carta.PALO.Espada)
            {
                c2 = 22;
            }
            else if (m1[1].valor == Carta.VALOR.Cinco && m1[1].palo == Carta.PALO.Espada)
            {
                c2 = 23;
            }
            else if (m1[1].valor == Carta.VALOR.Cuatro && m1[1].palo == Carta.PALO.Espada)
            {
                c2 = 24;
            }
            else if (m1[1].valor == Carta.VALOR.Tres && m1[1].palo == Carta.PALO.Espada)
            {
                c2 = 25;
            }
            else if (m1[1].valor == Carta.VALOR.Dos && m1[1].palo == Carta.PALO.Espada)
            {
                c2 = 26;
            }

            else if (m1[1].valor == Carta.VALOR.As && m1[1].palo == Carta.PALO.Diamante)
            {
                c2 = 27;
            }
            else if (m1[1].valor == Carta.VALOR.Rey && m1[1].palo == Carta.PALO.Diamante)
            {
                c2 = 28;
            }
            else if (m1[1].valor == Carta.VALOR.Reina && m1[1].palo == Carta.PALO.Diamante)
            {
                c2 = 29;
            }
            else if (m1[1].valor == Carta.VALOR.Joker && m1[1].palo == Carta.PALO.Diamante)
            {
                c2 = 30;
            }
            else if (m1[1].valor == Carta.VALOR.Diez && m1[1].palo == Carta.PALO.Diamante)
            {
                c2 = 31;
            }
            else if (m1[1].valor == Carta.VALOR.Nueve && m1[1].palo == Carta.PALO.Diamante)
            {
                c2 = 32;
            }
            else if (m1[1].valor == Carta.VALOR.Ocho && m1[1].palo == Carta.PALO.Diamante)
            {
                c2 = 33;
            }
            else if (m1[1].valor == Carta.VALOR.Siete && m1[1].palo == Carta.PALO.Diamante)
            {
                c2 = 34;
            }
            else if (m1[1].valor == Carta.VALOR.Seis && m1[1].palo == Carta.PALO.Diamante)
            {
                c2 = 35;
            }
            else if (m1[1].valor == Carta.VALOR.Cinco && m1[1].palo == Carta.PALO.Diamante)
            {
                c2 = 36;
            }
            else if (m1[1].valor == Carta.VALOR.Cuatro && m1[1].palo == Carta.PALO.Diamante)
            {
                c2 = 37;
            }
            else if (m1[1].valor == Carta.VALOR.Tres && m1[1].palo == Carta.PALO.Diamante)
            {
                c2 = 38;
            }
            else if (m1[1].valor == Carta.VALOR.Dos && m1[1].palo == Carta.PALO.Diamante)
            {
                c2 = 39;
            }

            else if (m1[1].valor == Carta.VALOR.As && m1[1].palo == Carta.PALO.Trebol)
            {
                c2 = 40;
            }
            else if (m1[1].valor == Carta.VALOR.Rey && m1[1].palo == Carta.PALO.Trebol)
            {
                c2 = 41;
            }
            else if (m1[1].valor == Carta.VALOR.Reina && m1[1].palo == Carta.PALO.Trebol)
            {
                c2 = 42;
            }
            else if (m1[1].valor == Carta.VALOR.Joker && m1[1].palo == Carta.PALO.Trebol)
            {
                c2 = 43;
            }
            else if (m1[1].valor == Carta.VALOR.Diez && m1[1].palo == Carta.PALO.Trebol)
            {
                c2 = 44;
            }
            else if (m1[1].valor == Carta.VALOR.Nueve && m1[1].palo == Carta.PALO.Trebol)
            {
                c2 = 45;
            }
            else if (m1[1].valor == Carta.VALOR.Ocho && m1[1].palo == Carta.PALO.Trebol)
            {
                c2 = 46;
            }
            else if (m1[1].valor == Carta.VALOR.Siete && m1[1].palo == Carta.PALO.Trebol)
            {
                c2 = 47;
            }
            else if (m1[1].valor == Carta.VALOR.Seis && m1[1].palo == Carta.PALO.Trebol)
            {
                c2 = 48;
            }
            else if (m1[1].valor == Carta.VALOR.Cinco && m1[1].palo == Carta.PALO.Trebol)
            {
                c2 = 49;
            }
            else if (m1[1].valor == Carta.VALOR.Cuatro && m1[1].palo == Carta.PALO.Trebol)
            {
                c2 = 50;
            }
            else if (m1[1].valor == Carta.VALOR.Tres && m1[1].palo == Carta.PALO.Trebol)
            {
                c2 = 51;
            }
            else if (m1[1].valor == Carta.VALOR.Dos && m1[1].palo == Carta.PALO.Trebol)
            {
                c2 = 52;
            }

            return c2;

        }

        public int RevisarManoC3(Carta[] m1)
        {
            int c3 = 0;


            if (m1[2].valor == Carta.VALOR.As && m1[2].palo == Carta.PALO.Corazon)
            {
                c3 = 1;
            }
            else if (m1[2].valor == Carta.VALOR.Rey && m1[2].palo == Carta.PALO.Corazon)
            {
                c3 = 2;
            }
            else if (m1[2].valor == Carta.VALOR.Reina && m1[2].palo == Carta.PALO.Corazon)
            {
                c3 = 3;
            }
            else if (m1[2].valor == Carta.VALOR.Joker && m1[2].palo == Carta.PALO.Corazon)
            {
                c3 = 4;
            }
            else if (m1[2].valor == Carta.VALOR.Diez && m1[2].palo == Carta.PALO.Corazon)
            {
                c3 = 5;
            }
            else if (m1[2].valor == Carta.VALOR.Nueve && m1[2].palo == Carta.PALO.Corazon)
            {
                c3 = 6;
            }
            else if (m1[2].valor == Carta.VALOR.Ocho && m1[2].palo == Carta.PALO.Corazon)
            {
                c3 = 7;
            }
            else if (m1[2].valor == Carta.VALOR.Siete && m1[2].palo == Carta.PALO.Corazon)
            {
                c3 = 8;
            }
            else if (m1[2].valor == Carta.VALOR.Seis && m1[2].palo == Carta.PALO.Corazon)
            {
                c3 = 9;
            }
            else if (m1[2].valor == Carta.VALOR.Cinco && m1[2].palo == Carta.PALO.Corazon)
            {
                c3 = 10;
            }
            else if (m1[2].valor == Carta.VALOR.Cuatro && m1[2].palo == Carta.PALO.Corazon)
            {
                c3 = 11;
            }
            else if (m1[2].valor == Carta.VALOR.Tres && m1[2].palo == Carta.PALO.Corazon)
            {
                c3 = 12;
            }
            else if (m1[2].valor == Carta.VALOR.Dos && m1[2].palo == Carta.PALO.Corazon)
            {
                c3 = 13;
            }

            else if (m1[2].valor == Carta.VALOR.As && m1[2].palo == Carta.PALO.Espada)
            {
                c3 = 14;
            }
            else if (m1[2].valor == Carta.VALOR.Rey && m1[2].palo == Carta.PALO.Espada)
            {
                c3 = 15;
            }
            else if (m1[2].valor == Carta.VALOR.Reina && m1[2].palo == Carta.PALO.Espada)
            {
                c3 = 16;
            }
            else if (m1[2].valor == Carta.VALOR.Joker && m1[2].palo == Carta.PALO.Espada)
            {
                c3 = 17;
            }
            else if (m1[2].valor == Carta.VALOR.Diez && m1[2].palo == Carta.PALO.Espada)
            {
                c3 = 18;
            }
            else if (m1[2].valor == Carta.VALOR.Nueve && m1[2].palo == Carta.PALO.Espada)
            {
                c3 = 19;
            }
            else if (m1[2].valor == Carta.VALOR.Ocho && m1[2].palo == Carta.PALO.Espada)
            {
                c3 = 20;
            }
            else if (m1[2].valor == Carta.VALOR.Siete && m1[2].palo == Carta.PALO.Espada)
            {
                c3 = 21;
            }
            else if (m1[2].valor == Carta.VALOR.Seis && m1[2].palo == Carta.PALO.Espada)
            {
                c3 = 22;
            }
            else if (m1[2].valor == Carta.VALOR.Cinco && m1[2].palo == Carta.PALO.Espada)
            {
                c3 = 23;
            }
            else if (m1[2].valor == Carta.VALOR.Cuatro && m1[2].palo == Carta.PALO.Espada)
            {
                c3 = 24;
            }
            else if (m1[2].valor == Carta.VALOR.Tres && m1[2].palo == Carta.PALO.Espada)
            {
                c3 = 25;
            }
            else if (m1[2].valor == Carta.VALOR.Dos && m1[2].palo == Carta.PALO.Espada)
            {
                c3 = 26;
            }

            else if (m1[2].valor == Carta.VALOR.As && m1[2].palo == Carta.PALO.Diamante)
            {
                c3 = 27;
            }
            else if (m1[2].valor == Carta.VALOR.Rey && m1[2].palo == Carta.PALO.Diamante)
            {
                c3 = 28;
            }
            else if (m1[2].valor == Carta.VALOR.Reina && m1[2].palo == Carta.PALO.Diamante)
            {
                c3 = 29;
            }
            else if (m1[2].valor == Carta.VALOR.Joker && m1[2].palo == Carta.PALO.Diamante)
            {
                c3 = 30;
            }
            else if (m1[2].valor == Carta.VALOR.Diez && m1[2].palo == Carta.PALO.Diamante)
            {
                c3 = 31;
            }
            else if (m1[2].valor == Carta.VALOR.Nueve && m1[2].palo == Carta.PALO.Diamante)
            {
                c3 = 32;
            }
            else if (m1[2].valor == Carta.VALOR.Ocho && m1[2].palo == Carta.PALO.Diamante)
            {
                c3 = 33;
            }
            else if (m1[2].valor == Carta.VALOR.Siete && m1[2].palo == Carta.PALO.Diamante)
            {
                c3 = 34;
            }
            else if (m1[2].valor == Carta.VALOR.Seis && m1[2].palo == Carta.PALO.Diamante)
            {
                c3 = 35;
            }
            else if (m1[2].valor == Carta.VALOR.Cinco && m1[2].palo == Carta.PALO.Diamante)
            {
                c3 = 36;
            }
            else if (m1[2].valor == Carta.VALOR.Cuatro && m1[2].palo == Carta.PALO.Diamante)
            {
                c3 = 37;
            }
            else if (m1[2].valor == Carta.VALOR.Tres && m1[2].palo == Carta.PALO.Diamante)
            {
                c3 = 38;
            }
            else if (m1[2].valor == Carta.VALOR.Dos && m1[2].palo == Carta.PALO.Diamante)
            {
                c3 = 39;
            }

            else if (m1[2].valor == Carta.VALOR.As && m1[2].palo == Carta.PALO.Trebol)
            {
                c3 = 40;
            }
            else if (m1[2].valor == Carta.VALOR.Rey && m1[2].palo == Carta.PALO.Trebol)
            {
                c3 = 41;
            }
            else if (m1[2].valor == Carta.VALOR.Reina && m1[2].palo == Carta.PALO.Trebol)
            {
                c3 = 42;
            }
            else if (m1[2].valor == Carta.VALOR.Joker && m1[2].palo == Carta.PALO.Trebol)
            {
                c3 = 43;
            }
            else if (m1[2].valor == Carta.VALOR.Diez && m1[2].palo == Carta.PALO.Trebol)
            {
                c3 = 44;
            }
            else if (m1[2].valor == Carta.VALOR.Nueve && m1[2].palo == Carta.PALO.Trebol)
            {
                c3 = 45;
            }
            else if (m1[2].valor == Carta.VALOR.Ocho && m1[2].palo == Carta.PALO.Trebol)
            {
                c3 = 46;
            }
            else if (m1[2].valor == Carta.VALOR.Siete && m1[2].palo == Carta.PALO.Trebol)
            {
                c3 = 47;
            }
            else if (m1[2].valor == Carta.VALOR.Seis && m1[2].palo == Carta.PALO.Trebol)
            {
                c3 = 48;
            }
            else if (m1[2].valor == Carta.VALOR.Cinco && m1[2].palo == Carta.PALO.Trebol)
            {
                c3 = 49;
            }
            else if (m1[2].valor == Carta.VALOR.Cuatro && m1[2].palo == Carta.PALO.Trebol)
            {
                c3 = 50;
            }
            else if (m1[2].valor == Carta.VALOR.Tres && m1[2].palo == Carta.PALO.Trebol)
            {
                c3 = 51;
            }
            else if (m1[2].valor == Carta.VALOR.Dos && m1[2].palo == Carta.PALO.Trebol)
            {
                c3 = 52;
            }

            return c3;

        }

        public int RevisarManoC4(Carta[] m1)
        {
            int c4 = 0;


            if (m1[3].valor == Carta.VALOR.As && m1[3].palo == Carta.PALO.Corazon)
            {
                c4 = 1;
            }
            else if (m1[3].valor == Carta.VALOR.Rey && m1[3].palo == Carta.PALO.Corazon)
            {
                c4 = 2;
            }
            else if (m1[3].valor == Carta.VALOR.Reina && m1[3].palo == Carta.PALO.Corazon)
            {
                c4 = 3;
            }
            else if (m1[3].valor == Carta.VALOR.Joker && m1[3].palo == Carta.PALO.Corazon)
            {
                c4 = 4;
            }
            else if (m1[3].valor == Carta.VALOR.Diez && m1[3].palo == Carta.PALO.Corazon)
            {
                c4 = 5;
            }
            else if (m1[3].valor == Carta.VALOR.Nueve && m1[3].palo == Carta.PALO.Corazon)
            {
                c4 = 6;
            }
            else if (m1[3].valor == Carta.VALOR.Ocho && m1[3].palo == Carta.PALO.Corazon)
            {
                c4 = 7;
            }
            else if (m1[3].valor == Carta.VALOR.Siete && m1[3].palo == Carta.PALO.Corazon)
            {
                c4 = 8;
            }
            else if (m1[3].valor == Carta.VALOR.Seis && m1[3].palo == Carta.PALO.Corazon)
            {
                c4 = 9;
            }
            else if (m1[3].valor == Carta.VALOR.Cinco && m1[3].palo == Carta.PALO.Corazon)
            {
                c4 = 10;
            }
            else if (m1[3].valor == Carta.VALOR.Cuatro && m1[3].palo == Carta.PALO.Corazon)
            {
                c4 = 11;
            }
            else if (m1[3].valor == Carta.VALOR.Tres && m1[3].palo == Carta.PALO.Corazon)
            {
                c4 = 12;
            }
            else if (m1[3].valor == Carta.VALOR.Dos && m1[3].palo == Carta.PALO.Corazon)
            {
                c4 = 13;
            }

            else if (m1[3].valor == Carta.VALOR.As && m1[3].palo == Carta.PALO.Espada)
            {
                c4 = 14;
            }
            else if (m1[3].valor == Carta.VALOR.Rey && m1[3].palo == Carta.PALO.Espada)
            {
                c4 = 15;
            }
            else if (m1[3].valor == Carta.VALOR.Reina && m1[3].palo == Carta.PALO.Espada)
            {
                c4 = 16;
            }
            else if (m1[3].valor == Carta.VALOR.Joker && m1[3].palo == Carta.PALO.Espada)
            {
                c4 = 17;
            }
            else if (m1[3].valor == Carta.VALOR.Diez && m1[3].palo == Carta.PALO.Espada)
            {
                c4 = 18;
            }
            else if (m1[3].valor == Carta.VALOR.Nueve && m1[3].palo == Carta.PALO.Espada)
            {
                c4 = 19;
            }
            else if (m1[3].valor == Carta.VALOR.Ocho && m1[3].palo == Carta.PALO.Espada)
            {
                c4 = 20;
            }
            else if (m1[3].valor == Carta.VALOR.Siete && m1[3].palo == Carta.PALO.Espada)
            {
                c4 = 21;
            }
            else if (m1[3].valor == Carta.VALOR.Seis && m1[3].palo == Carta.PALO.Espada)
            {
                c4 = 22;
            }
            else if (m1[3].valor == Carta.VALOR.Cinco && m1[3].palo == Carta.PALO.Espada)
            {
                c4 = 23;
            }
            else if (m1[3].valor == Carta.VALOR.Cuatro && m1[3].palo == Carta.PALO.Espada)
            {
                c4 = 24;
            }
            else if (m1[3].valor == Carta.VALOR.Tres && m1[3].palo == Carta.PALO.Espada)
            {
                c4 = 25;
            }
            else if (m1[3].valor == Carta.VALOR.Dos && m1[3].palo == Carta.PALO.Espada)
            {
                c4 = 26;
            }

            else if (m1[3].valor == Carta.VALOR.As && m1[3].palo == Carta.PALO.Diamante)
            {
                c4 = 27;
            }
            else if (m1[3].valor == Carta.VALOR.Rey && m1[3].palo == Carta.PALO.Diamante)
            {
                c4 = 28;
            }
            else if (m1[3].valor == Carta.VALOR.Reina && m1[3].palo == Carta.PALO.Diamante)
            {
                c4 = 29;
            }
            else if (m1[3].valor == Carta.VALOR.Joker && m1[3].palo == Carta.PALO.Diamante)
            {
                c4 = 30;
            }
            else if (m1[3].valor == Carta.VALOR.Diez && m1[3].palo == Carta.PALO.Diamante)
            {
                c4 = 31;
            }
            else if (m1[3].valor == Carta.VALOR.Nueve && m1[3].palo == Carta.PALO.Diamante)
            {
                c4 = 32;
            }
            else if (m1[3].valor == Carta.VALOR.Ocho && m1[3].palo == Carta.PALO.Diamante)
            {
                c4 = 33;
            }
            else if (m1[3].valor == Carta.VALOR.Siete && m1[3].palo == Carta.PALO.Diamante)
            {
                c4 = 34;
            }
            else if (m1[3].valor == Carta.VALOR.Seis && m1[3].palo == Carta.PALO.Diamante)
            {
                c4 = 35;
            }
            else if (m1[3].valor == Carta.VALOR.Cinco && m1[3].palo == Carta.PALO.Diamante)
            {
                c4 = 36;
            }
            else if (m1[3].valor == Carta.VALOR.Cuatro && m1[3].palo == Carta.PALO.Diamante)
            {
                c4 = 37;
            }
            else if (m1[3].valor == Carta.VALOR.Tres && m1[3].palo == Carta.PALO.Diamante)
            {
                c4 = 38;
            }
            else if (m1[3].valor == Carta.VALOR.Dos && m1[3].palo == Carta.PALO.Diamante)
            {
                c4 = 39;
            }

            else if (m1[3].valor == Carta.VALOR.As && m1[3].palo == Carta.PALO.Trebol)
            {
                c4 = 40;
            }
            else if (m1[3].valor == Carta.VALOR.Rey && m1[3].palo == Carta.PALO.Trebol)
            {
                c4 = 41;
            }
            else if (m1[3].valor == Carta.VALOR.Reina && m1[3].palo == Carta.PALO.Trebol)
            {
                c4 = 42;
            }
            else if (m1[3].valor == Carta.VALOR.Joker && m1[3].palo == Carta.PALO.Trebol)
            {
                c4 = 43;
            }
            else if (m1[3].valor == Carta.VALOR.Diez && m1[3].palo == Carta.PALO.Trebol)
            {
                c4 = 44;
            }
            else if (m1[3].valor == Carta.VALOR.Nueve && m1[3].palo == Carta.PALO.Trebol)
            {
                c4 = 45;
            }
            else if (m1[3].valor == Carta.VALOR.Ocho && m1[3].palo == Carta.PALO.Trebol)
            {
                c4 = 46;
            }
            else if (m1[3].valor == Carta.VALOR.Siete && m1[3].palo == Carta.PALO.Trebol)
            {
                c4 = 47;
            }
            else if (m1[3].valor == Carta.VALOR.Seis && m1[3].palo == Carta.PALO.Trebol)
            {
                c4 = 48;
            }
            else if (m1[3].valor == Carta.VALOR.Cinco && m1[3].palo == Carta.PALO.Trebol)
            {
                c4 = 49;
            }
            else if (m1[3].valor == Carta.VALOR.Cuatro && m1[3].palo == Carta.PALO.Trebol)
            {
                c4 = 50;
            }
            else if (m1[3].valor == Carta.VALOR.Tres && m1[3].palo == Carta.PALO.Trebol)
            {
                c4 = 51;
            }
            else if (m1[3].valor == Carta.VALOR.Dos && m1[3].palo == Carta.PALO.Trebol)
            {
                c4 = 52;
            }

            return c4;

        }

        public int RevisarManoC5(Carta[] m1)
        {
            int c5 = 0;


            if (m1[4].valor == Carta.VALOR.As && m1[4].palo == Carta.PALO.Corazon)
            {
                c5 = 1;
            }
            else if (m1[4].valor == Carta.VALOR.Rey && m1[4].palo == Carta.PALO.Corazon)
            {
                c5 = 2;
            }
            else if (m1[4].valor == Carta.VALOR.Reina && m1[4].palo == Carta.PALO.Corazon)
            {
                c5 = 3;
            }
            else if (m1[4].valor == Carta.VALOR.Joker && m1[4].palo == Carta.PALO.Corazon)
            {
                c5 = 4;
            }
            else if (m1[4].valor == Carta.VALOR.Diez && m1[4].palo == Carta.PALO.Corazon)
            {
                c5 = 5;
            }
            else if (m1[4].valor == Carta.VALOR.Nueve && m1[4].palo == Carta.PALO.Corazon)
            {
                c5 = 6;
            }
            else if (m1[4].valor == Carta.VALOR.Ocho && m1[4].palo == Carta.PALO.Corazon)
            {
                c5 = 7;
            }
            else if (m1[4].valor == Carta.VALOR.Siete && m1[4].palo == Carta.PALO.Corazon)
            {
                c5 = 8;
            }
            else if (m1[4].valor == Carta.VALOR.Seis && m1[4].palo == Carta.PALO.Corazon)
            {
                c5 = 9;
            }
            else if (m1[4].valor == Carta.VALOR.Cinco && m1[4].palo == Carta.PALO.Corazon)
            {
                c5 = 10;
            }
            else if (m1[4].valor == Carta.VALOR.Cuatro && m1[4].palo == Carta.PALO.Corazon)
            {
                c5 = 11;
            }
            else if (m1[4].valor == Carta.VALOR.Tres && m1[4].palo == Carta.PALO.Corazon)
            {
                c5 = 12;
            }
            else if (m1[4].valor == Carta.VALOR.Dos && m1[4].palo == Carta.PALO.Corazon)
            {
                c5 = 13;
            }

            else if (m1[4].valor == Carta.VALOR.As && m1[4].palo == Carta.PALO.Espada)
            {
                c5 = 14;
            }
            else if (m1[4].valor == Carta.VALOR.Rey && m1[4].palo == Carta.PALO.Espada)
            {
                c5 = 15;
            }
            else if (m1[4].valor == Carta.VALOR.Reina && m1[4].palo == Carta.PALO.Espada)
            {
                c5 = 16;
            }
            else if (m1[4].valor == Carta.VALOR.Joker && m1[4].palo == Carta.PALO.Espada)
            {
                c5 = 17;
            }
            else if (m1[4].valor == Carta.VALOR.Diez && m1[4].palo == Carta.PALO.Espada)
            {
                c5 = 18;
            }
            else if (m1[4].valor == Carta.VALOR.Nueve && m1[4].palo == Carta.PALO.Espada)
            {
                c5 = 19;
            }
            else if (m1[4].valor == Carta.VALOR.Ocho && m1[4].palo == Carta.PALO.Espada)
            {
                c5 = 20;
            }
            else if (m1[4].valor == Carta.VALOR.Siete && m1[4].palo == Carta.PALO.Espada)
            {
                c5 = 21;
            }
            else if (m1[4].valor == Carta.VALOR.Seis && m1[4].palo == Carta.PALO.Espada)
            {
                c5 = 22;
            }
            else if (m1[4].valor == Carta.VALOR.Cinco && m1[4].palo == Carta.PALO.Espada)
            {
                c5 = 23;
            }
            else if (m1[4].valor == Carta.VALOR.Cuatro && m1[4].palo == Carta.PALO.Espada)
            {
                c5 = 24;
            }
            else if (m1[4].valor == Carta.VALOR.Tres && m1[4].palo == Carta.PALO.Espada)
            {
                c5 = 25;
            }
            else if (m1[4].valor == Carta.VALOR.Dos && m1[4].palo == Carta.PALO.Espada)
            {
                c5 = 26;
            }

            else if (m1[4].valor == Carta.VALOR.As && m1[4].palo == Carta.PALO.Diamante)
            {
                c5 = 27;
            }
            else if (m1[4].valor == Carta.VALOR.Rey && m1[4].palo == Carta.PALO.Diamante)
            {
                c5 = 28;
            }
            else if (m1[4].valor == Carta.VALOR.Reina && m1[4].palo == Carta.PALO.Diamante)
            {
                c5 = 29;
            }
            else if (m1[4].valor == Carta.VALOR.Joker && m1[4].palo == Carta.PALO.Diamante)
            {
                c5 = 30;
            }
            else if (m1[4].valor == Carta.VALOR.Diez && m1[4].palo == Carta.PALO.Diamante)
            {
                c5 = 31;
            }
            else if (m1[4].valor == Carta.VALOR.Nueve && m1[4].palo == Carta.PALO.Diamante)
            {
                c5 = 32;
            }
            else if (m1[4].valor == Carta.VALOR.Ocho && m1[4].palo == Carta.PALO.Diamante)
            {
                c5 = 33;
            }
            else if (m1[4].valor == Carta.VALOR.Siete && m1[4].palo == Carta.PALO.Diamante)
            {
                c5 = 34;
            }
            else if (m1[4].valor == Carta.VALOR.Seis && m1[4].palo == Carta.PALO.Diamante)
            {
                c5 = 35;
            }
            else if (m1[4].valor == Carta.VALOR.Cinco && m1[4].palo == Carta.PALO.Diamante)
            {
                c5 = 36;
            }
            else if (m1[4].valor == Carta.VALOR.Cuatro && m1[4].palo == Carta.PALO.Diamante)
            {
                c5 = 37;
            }
            else if (m1[4].valor == Carta.VALOR.Tres && m1[4].palo == Carta.PALO.Diamante)
            {
                c5 = 38;
            }
            else if (m1[4].valor == Carta.VALOR.Dos && m1[4].palo == Carta.PALO.Diamante)
            {
                c5 = 39;
            }

            else if (m1[4].valor == Carta.VALOR.As && m1[4].palo == Carta.PALO.Trebol)
            {
                c5 = 40;
            }
            else if (m1[4].valor == Carta.VALOR.Rey && m1[4].palo == Carta.PALO.Trebol)
            {
                c5 = 41;
            }
            else if (m1[4].valor == Carta.VALOR.Reina && m1[4].palo == Carta.PALO.Trebol)
            {
                c5 = 42;
            }
            else if (m1[4].valor == Carta.VALOR.Joker && m1[4].palo == Carta.PALO.Trebol)
            {
                c5 = 43;
            }
            else if (m1[4].valor == Carta.VALOR.Diez && m1[4].palo == Carta.PALO.Trebol)
            {
                c5 = 44;
            }
            else if (m1[4].valor == Carta.VALOR.Nueve && m1[4].palo == Carta.PALO.Trebol)
            {
                c5 = 45;
            }
            else if (m1[4].valor == Carta.VALOR.Ocho && m1[4].palo == Carta.PALO.Trebol)
            {
                c5 = 46;
            }
            else if (m1[4].valor == Carta.VALOR.Siete && m1[4].palo == Carta.PALO.Trebol)
            {
                c5 = 47;
            }
            else if (m1[4].valor == Carta.VALOR.Seis && m1[4].palo == Carta.PALO.Trebol)
            {
                c5 = 48;
            }
            else if (m1[4].valor == Carta.VALOR.Cinco && m1[4].palo == Carta.PALO.Trebol)
            {
                c5 = 49;
            }
            else if (m1[4].valor == Carta.VALOR.Cuatro && m1[4].palo == Carta.PALO.Trebol)
            {
                c5 = 50;
            }
            else if (m1[4].valor == Carta.VALOR.Tres && m1[4].palo == Carta.PALO.Trebol)
            {
                c5 = 51;
            }
            else if (m1[4].valor == Carta.VALOR.Dos && m1[4].palo == Carta.PALO.Trebol)
            {
                c5 = 52;
            }

            return c5;

        }

        public Carta[] Descarte(int selec1, int selec2, int selec3, int selec4, int selec5, Carta[] desc)
        {
            
            desca[1] = desc[1];
            desca[2] = desc[2];
            desca[3] = desc[3];
            desca[4] = desc[4];

            if (selec1==1 && selec2==0 && selec3 == 0 && selec4 == 0 && selec5==0)
            {
                desca[0] = obtenerbaraja[20];
                
                return desca;
                
            }

          /*  else if (selec1 == 0 && selec2 == 1 && selec3 == 0 && selec4 == 0 && selec5 == 0)
            {
                desc[1] = obtenerbaraja[20];
                return desc;
            }

            else if (selec1 == 0 && selec2 == 0 && selec3 == 1 && selec4 == 0 && selec5 == 0)
            {
                desc[2] = obtenerbaraja[20];
                return desc;
            }

            else if (selec1 == 0 && selec2 == 0 && selec3 == 0 && selec4 == 1 && selec5 == 0)
            {
                desc[3] = obtenerbaraja[20];
                return desc;
            }

            else if (selec1 == 0 && selec2 == 0 && selec3 == 0 && selec4 == 0 && selec5 == 1)
            {
                desc[4] = obtenerbaraja[20];
                return desc;
            }*/


            return desca;
        }
    




    }
}
