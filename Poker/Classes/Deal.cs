﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker.Classes
{
    class Deal:Baraja
    {
        Carta mano = new Carta();
        Baraja baraja = new Baraja();
        
        private Carta[] manoP1;
        private Carta[] manoP2;
        private Carta[] manoP3;
        private Carta[] manoP4;
        private Carta[] desca;

        //ordenadas
        private Carta[] manoP1ord;
        private Carta[] manoP2ord;
        private Carta[] manoP3ord;
        private Carta[] manoP4ord;

        private Carta[] em1;
        private Carta[] em2;
        private Carta[] em3;
        private Carta[] em4;

        private Carta[] mo1;
        private Carta[] mo2;
        private Carta[] mo3;
        private Carta[] mo4;

        private Carta[] man1;
        private Carta[] manp1;



        public Deal()
        {
            manoP1 = new Carta[5];
            manoP2 = new Carta[5];
            manoP3 = new Carta[5];
            manoP4 = new Carta[5];

            manoP1ord = new Carta[5];
            manoP2ord = new Carta[5];
            manoP3ord = new Carta[5];
            manoP4ord = new Carta[5];

            desca = new Carta[5];
            man1 = new Carta[5];
            manp1 = new Carta[5];
            em1 = new Carta[5];
            em2 = new Carta[5];
            em3 = new Carta[5];
            em4 = new Carta[5];

            mo2 = new Carta[5];
            mo2 = new Carta[5];
            mo3 = new Carta[5];
            mo4 = new Carta[5];



        }

      
        public void Repartir()
        {
            SetBaraja();
            barajear();
            

            // EvaluarMano();

        }

        public Carta[] ObtenerMano1()
        {
            for (int i = 0; i < 5; i++)
            {
                manoP1[i] = obtenerbaraja[i];
            }
            return manoP1;
        }

        public Carta[] ObtenerMano2()
        { 
            for (int i = 5; i < 10; i++)
            {
                manoP2[i-5] = obtenerbaraja[i];
            }
            return manoP2;
        }

        public Carta[] ObtenerMano3()
        { 
            for (int i = 10; i < 15; i++)
            {
                manoP3[i-10] = obtenerbaraja[i];
            }
            return manoP3;
        }

        public Carta[] ObtenerMano4()
        { 
            for (int i = 15; i < 20; i++)
            {
                manoP4[i-15] = obtenerbaraja[i];
            }
            return manoP4;
        }




        






        public Carta[] Descartee(int selec1, int selec2, int selec3, int selec4, int selec5, Carta[] desc)
        {

           // desca[1] = desc[1];
            //desca[2] = desc[2];
            //desca[3] = desc[3];
            //desca[4] = desc[4];

            if (selec1 == 1 && selec2 == 0 && selec3 == 0 && selec4 == 0 && selec5 == 0)
            {
                desca[0] = obtenerbaraja[20];
                desca[1] = desc[1];
                desca[2] = desc[2];
                desca[3] = desc[3];
                desca[4] = desc[4];
                  

                return desca;

            }
            else if (selec1 == 0 && selec2 == 1 && selec3 == 0 && selec4 == 0 && selec5 == 0)
            {
                desca[0] = desc[0];
                desca[1] = obtenerbaraja[20];
                desca[2] = desc[2];
                desca[3] = desc[3];
                desca[4] = desc[4];

                return desca;

            }

            else if (selec1 == 0 && selec2 == 0 && selec3 == 1 && selec4 == 0 && selec5 == 0)
            {
                desca[0] = desc[0];
                desca[1] = desc[1];
                desca[2] = obtenerbaraja[20]; 
                desca[3] = desc[3];
                desca[4] = desc[4];

                return desca;

            }

            else if (selec1 == 0 && selec2 == 0 && selec3 == 0 && selec4 == 1 && selec5 == 0)
            {
                desca[0] = desc[0];
                desca[1] = desc[1];
                desca[2] = desc[2];
                desca[3] = obtenerbaraja[20]; 
                desca[4] = desc[4];

                return desca;

            }

            else if (selec1 == 0 && selec2 == 0 && selec3 == 0 && selec4 == 0 && selec5 == 1)
            {
                desca[0] = desc[0];
                desca[1] = desc[1];
                desca[2] = desc[2];
                desca[3] = desc[3];
                desca[4] = obtenerbaraja[20];

                return desca;

            }

            else if (selec1 == 1 && selec2 == 1 && selec3 == 0 && selec4 == 0 && selec5 == 0)
            {
                desca[0] = obtenerbaraja[20]; 
                desca[1] = obtenerbaraja[21]; 
                desca[2] = desc[2];
                desca[3] = desc[3];
                desca[4] = desc[4];

                return desca;

            }

            else if (selec1 == 1 && selec2 == 0 && selec3 == 1 && selec4 == 0 && selec5 == 0)
            {
                desca[0] = obtenerbaraja[20]; 
                desca[1] = desc[1];
                desca[2] = obtenerbaraja[21]; 
                desca[3] = desc[3];
                desca[4] = desc[4];

                return desca;

            }

            else if (selec1 == 1 && selec2 == 0 && selec3 == 0 && selec4 == 1 && selec5 == 0)
            {
                desca[0] = obtenerbaraja[20]; 
                desca[1] = desc[1];
                desca[2] = desc[2];
                desca[3] = obtenerbaraja[21];
                desca[4] = desc[4];

                return desca;

            }

            else if (selec1 == 1 && selec2 == 0 && selec3 == 0 && selec4 == 0 && selec5 == 1)
            {
                desca[0] = obtenerbaraja[20]; 
                desca[1] = desc[1];
                desca[2] = desc[2];
                desca[3] = desc[3];
                desca[4] = obtenerbaraja[21];

                return desca;

            }

            else if (selec1 == 0 && selec2 == 1 && selec3 == 1 && selec4 == 0 && selec5 == 0)
            {
                desca[0] = desc[0];
                desca[1] = obtenerbaraja[20]; 
                desca[2] = obtenerbaraja[21]; 
                desca[3] = desc[3];
                desca[4] = desc[4];

                return desca;

            }

            else if (selec1 == 0 && selec2 == 1 && selec3 == 0 && selec4 == 1 && selec5 == 0)
            {
                desca[0] = desc[0];
                desca[1] = obtenerbaraja[20]; 
                desca[2] = desc[2];
                desca[3] = obtenerbaraja[21];
                desca[4] = desc[4];

                return desca;

            }

            else if (selec1 == 0 && selec2 == 1 && selec3 == 0 && selec4 == 0 && selec5 == 1)
            {
                desca[0] = desc[0];
                desca[1] = obtenerbaraja[20]; 
                desca[2] = desc[2];
                desca[3] = desc[3];
                desca[4] = obtenerbaraja[21];

                return desca;

            }

            else if (selec1 == 0 && selec2 == 0 && selec3 == 1 && selec4 == 1 && selec5 == 0)
            {
                desca[0] = desc[0];
                desca[1] = desc[1];
                desca[2] = obtenerbaraja[20];
                desca[3] = obtenerbaraja[20];
                desca[4] = desc[4];

                return desca;

            }

            else if (selec1 == 0 && selec2 == 0 && selec3 == 1 && selec4 == 0 && selec5 == 1)
            {
                desca[0] = desc[0];
                desca[1] = desc[1];
                desca[2] = obtenerbaraja[20]; 
                desca[3] = desc[3];
                desca[4] = obtenerbaraja[21];

                return desca;

            }

            else if (selec1 == 0 && selec2 == 0 && selec3 == 0 && selec4 == 1 && selec5 == 1)
            {
                desca[0] = desc[0];
                desca[1] = desc[1];
                desca[2] = desc[2];
                desca[3] = obtenerbaraja[20]; 
                desca[4] = obtenerbaraja[21];

                return desca;

            }

            else if (selec1 == 1 && selec2 == 1 && selec3 == 1 && selec4 == 0 && selec5 == 0)
            {
                desca[0] = obtenerbaraja[20]; 
                desca[1] = obtenerbaraja[21]; 
                desca[2] = obtenerbaraja[22]; 
                desca[3] = desc[3];
                desca[4] = desc[4];

                return desca;

            }

            else if (selec1 == 1 && selec2 == 1 && selec3 == 0 && selec4 == 1 && selec5 == 0)
            {
                desca[0] = obtenerbaraja[20];
                desca[1] = obtenerbaraja[21];
                desca[2] = desc[2];
                desca[3] = obtenerbaraja[22];
                desca[4] = desc[4];

                return desca;

            }

            else if (selec1 == 1 && selec2 == 1 && selec3 == 0 && selec4 == 0 && selec5 == 1)
            {
                desca[0] = obtenerbaraja[20];
                desca[1] = obtenerbaraja[21];
                desca[2] = desc[2];
                desca[3] = desc[3];
                desca[4] = obtenerbaraja[22];

                return desca;

            }

            else if (selec1 == 1 && selec2 == 0 && selec3 == 1 && selec4 == 1 && selec5 == 0)
            {
                desca[0] = obtenerbaraja[20];
                desca[1] = desc[1];
                desca[2] = obtenerbaraja[21]; 
                desca[3] = obtenerbaraja[22];
                desca[4] = desc[4] ;

                return desca;

            }

            else if (selec1 == 1 && selec2 == 0 && selec3 == 1 && selec4 == 0 && selec5 == 1)
            {
                desca[0] = obtenerbaraja[20];
                desca[1] = desc[1];
                desca[2] = obtenerbaraja[21];
                desca[3] = desc[3];
                desca[4] = obtenerbaraja[22]; 

                return desca;

            }

            else if (selec1 == 1 && selec2 == 0 && selec3 == 0 && selec4 == 1 && selec5 == 1)
            {
                desca[0] = obtenerbaraja[20];
                desca[1] = desc[1];
                desca[2] = desc[2];
                desca[3] = obtenerbaraja[21];
                desca[4] = obtenerbaraja[22];

                return desca;

            }

            else if (selec1 == 0 && selec2 == 1 && selec3 == 1 && selec4 == 1 && selec5 == 0)
            {
                desca[0] = desc[0];
                desca[1] = obtenerbaraja[20];
                desca[2] = obtenerbaraja[21];
                desca[3] = obtenerbaraja[22];
                desca[4] = desc[4];

                return desca;

            }

            else if (selec1 == 0 && selec2 == 1 && selec3 == 1 && selec4 == 0 && selec5 == 1)
            {
                desca[0] = desc[0];
                desca[1] = obtenerbaraja[20];
                desca[2] = obtenerbaraja[21];
                desca[3] = desc[3];
                desca[4] = obtenerbaraja[22]; 

                return desca;

            }

            else if (selec1 == 0 && selec2 == 1 && selec3 == 0 && selec4 == 1 && selec5 == 1)
            {
                desca[0] = desc[0];
                desca[1] = obtenerbaraja[20];
                desca[2] = desc[2];
                desca[3] = obtenerbaraja[21]; 
                desca[4] = obtenerbaraja[22];

                return desca;

            }

            else if (selec1 == 0 && selec2 == 0 && selec3 == 1 && selec4 == 1 && selec5 == 1)
            {
                desca[0] = desc[0];
                desca[1] = desc[1];
                desca[2] = obtenerbaraja[20]; 
                desca[3] = obtenerbaraja[21];
                desca[4] = obtenerbaraja[22];

                return desca;

            }




       


            return desca;
        }






        public Carta[] OrdenarManoo(Carta[] m1)
        {
            man1 = m1;
            for (int i = 0; i < man1.Length - 1; i++)
            {
                int min = i;
                for (int j = i + 1; j < man1.Length; j++)
                    if ((int)man1[j].valor < (int)man1[min].valor) min = j;
                Carta temp = man1[i];
                man1[i] = man1[min];
                //int ki = (int)manoP1[min].valor;
                man1[min] = temp;

            }

            return man1;
        }

        public Carta[] OrdenarManoo1(Carta[] m1)
        {
            mo1 = m1;
            for (int i = 0; i < mo1.Length - 1; i++)
            {
                int min = i;
                for (int j = i + 1; j < mo1.Length; j++)
                    if ((int)mo1[j].valor < (int)mo1[min].valor) min = j;
                Carta temp = mo1[i];
                mo1[i] = mo1[min];
                //int ki = (int)manoP1[min].valor;
                mo1[min] = temp;

            }

            return mo1;
        }

        public Carta[] OrdenarManoo2(Carta[] m2)
        {
            mo2 = m2;
            for (int i = 0; i < mo2.Length - 1; i++)
            {
                int min = i;
                for (int j = i + 1; j < mo2.Length; j++)
                    if ((int)mo2[j].valor < (int)mo2[min].valor) min = j;
                Carta temp = mo2[i];
                mo2[i] = mo2[min];
                //int ki = (int)manoP1[min].valor;
                mo2[min] = temp;

            }

            return mo2;
        }

        public Carta[] OrdenarManoo3(Carta[] m3)
        {
            mo3 = m3;
            for (int i = 0; i < mo3.Length - 1; i++)
            {
                int min = i;
                for (int j = i + 1; j < mo3.Length; j++)
                    if ((int)mo3[j].valor < (int)mo3[min].valor) min = j;
                Carta temp = mo3[i];
                mo3[i] = mo3[min];
                //int ki = (int)manoP1[min].valor;
                mo3[min] = temp;

            }

            return mo3;
        }

        public Carta[] OrdenarManoo4(Carta[] m4)
        {
            mo4 = m4;
            for (int i = 0; i < mo4.Length - 1; i++)
            {
                int min = i;
                for (int j = i + 1; j < mo4.Length; j++)
                    if ((int)mo4[j].valor < (int)mo4[min].valor) min = j;
                Carta temp = mo4[i];
                mo4[i] = mo4[min];
                //int ki = (int)manoP1[min].valor;
                mo4[min] = temp;

            }

            return mo4;
        }




        public string MostrarManoP1()
        {
            string mp1 = manoP1[0].ToString();

            return mp1;
        }

        public int EvaluarMano(Carta[] map1)
        {
            manp1 = map1;
            em1 = OrdenarManoo1(manp1);
            em2 = OrdenarManoo2(manoP2);
            em3 = OrdenarManoo3(manoP3);
            em4 = OrdenarManoo4(manoP4);
            

            int win;
            //create computer's player's evaluation objects (passing SORTED hand to constructor)
            Evaluacion evaluador = new Evaluacion();
           

             Mano manop1 = evaluador.EvaluarMano(em1);
             Mano manop2 = evaluador.EvaluarMano(em2);
             Mano manop3 = evaluador.EvaluarMano(em3);
             Mano manop4 = evaluador.EvaluarMano(em4);




            if (manop1 > manop2 && manop1 > manop3 && manop1 > manop4)
            {
                win = 1;
                return win;
            }

            else if (manop2 > manop1 && manop2 > manop3 && manop2 > manop4)
            {
                win = 2;
                return win;
            }

            else if (manop3 > manop2 && manop3 > manop1 && manop3 > manop4)
            {
                win = 3;
                return win;
            }

            else if (manop4 > manop2 && manop4 > manop1 && manop4 > manop3)
            {
                win = 4;
                return win;
            }
            else
                win = 0;
           

            return win;
        }


        public string EvaluarManoo(Carta[] map1)
        {
            manp1 = map1;
            em1 = OrdenarManoo1(manp1);
            em2 = OrdenarManoo2(manoP2);
            em3 = OrdenarManoo3(manoP3);
            em4 = OrdenarManoo4(manoP4);


            string wini;
            //create computer's player's evaluation objects (passing SORTED hand to constructor)
            Evaluacion evaluador = new Evaluacion();


            Mano manop1 = evaluador.EvaluarMano(em1);
            Mano manop2 = evaluador.EvaluarMano(em2);
            Mano manop3 = evaluador.EvaluarMano(em3);
            Mano manop4 = evaluador.EvaluarMano(em4);




            wini= manop1.ToString();

            return wini;
        }

        public int NombreMano1(Carta[] mno)
        {
            manp1 = mno;
            em1 = OrdenarManoo1(manp1);
            


            int nom = 0;
            //create computer's player's evaluation objects (passing SORTED hand to constructor)
            Evaluacion evaluador = new Evaluacion();


            Mano manop1 = evaluador.EvaluarMano(em1);
       
            if (manop1 == Mano.Nada)
            {
                nom = 0;
                return nom;

            }
            else if (manop1 == Mano.UnPar)
            {
                nom = 1;
                return nom;

            }
            else if (manop1 == Mano.DoblePar)
            {
                nom = 2;
                return nom;

            }
            else if (manop1 == Mano.Trio)
            {
                nom = 3;
                return nom;

            }
            else if (manop1 == Mano.Escalera)
            {
                nom = 4;
                return nom;

            }
            else if (manop1 == Mano.Color)
            {
                nom = 5;
                return nom;

            }
            else if (manop1 == Mano.Full)
            {
                nom = 6;
                return nom;

            }

            else if (manop1 == Mano.Poker)
            {
                nom = 7;
                return nom;

            }
            else if (manop1 == Mano.EscaleraColor)
            {
                nom = 8;
                return nom;

            }
            else if (manop1 == Mano.EscaleraReal)
            {
                nom = 9;
                return nom;

            }
           
            return nom;

        }

        public int NombreMano2()
        {
           
            em2 = OrdenarManoo2(manoP2);
            


            int nom2 = 0;
            //create computer's player's evaluation objects (passing SORTED hand to constructor)
            Evaluacion evaluador = new Evaluacion();


           
            Mano manop2 = evaluador.EvaluarMano(em2);
            
            
            if (manop2 == Mano.Nada)
            {
                nom2 = 10;
                return nom2;

            }
            else if (manop2 == Mano.UnPar)
            {
                nom2 = 11;
                return nom2;

            }
            else if (manop2 == Mano.DoblePar)
            {
                nom2 = 12;
                return nom2;

            }
            else if (manop2 == Mano.Trio)
            {
                nom2 = 13;
                return nom2;

            }
            else if (manop2 == Mano.Escalera)
            {
                nom2 = 14;
                return nom2;

            }
            else if (manop2 == Mano.Color)
            {
                nom2 = 15;
                return nom2;

            }
            else if (manop2 == Mano.Full)
            {
                nom2 = 16;
                return nom2;

            }

            else if (manop2 == Mano.Poker)
            {
                nom2 = 17;
                return nom2;

            }
            else if (manop2 == Mano.EscaleraColor)
            {
                nom2 = 18;
                return nom2;

            }
            else if (manop2 == Mano.EscaleraReal)
            {
                nom2 = 19;
                return nom2;

            }
            
            
            return nom2;

        }

        public int NombreMano3()
        {
            
            em3 = OrdenarManoo3(manoP3);
        


            int nom3 = 0;
            //create computer's player's evaluation objects (passing SORTED hand to constructor)
            Evaluacion evaluador = new Evaluacion();


           
            Mano manop3 = evaluador.EvaluarMano(em3);
            
            
            if (manop3 == Mano.Nada)
            {
                nom3 = 20;
                return nom3;

            }
            else if (manop3 == Mano.UnPar)
            {
                nom3 = 21;
                return nom3;

            }
            else if (manop3 == Mano.DoblePar)
            {
                nom3 = 22;
                return nom3;

            }
            else if (manop3 == Mano.Trio)
            {
                nom3 = 23;
                return nom3;

            }
            else if (manop3 == Mano.Escalera)
            {
                nom3 = 24;
                return nom3;

            }
            else if (manop3 == Mano.Color)
            {
                nom3 = 25;
                return nom3;

            }
            else if (manop3 == Mano.Full)
            {
                nom3 = 26;
                return nom3;

            }

            else if (manop3 == Mano.Poker)
            {
                nom3 = 27;
                return nom3;

            }
            else if (manop3 == Mano.EscaleraColor)
            {
                nom3 = 28;
                return nom3;

            }
            else if (manop3 == Mano.EscaleraReal)
            {
                nom3 = 29;
                return nom3;

            }
            
            
            return nom3;

        }

        public int NombreMano4()
        {
           
            em4 = OrdenarManoo4(manoP4);


            int nom4 = 0;
            //create computer's player's evaluation objects (passing SORTED hand to constructor)
            Evaluacion evaluador = new Evaluacion();


            
            Mano manop4 = evaluador.EvaluarMano(em4);
            
            if (manop4 == Mano.Nada)
            {
                nom4 = 30;
                return nom4;

            }
            else if (manop4 == Mano.UnPar)
            {
                nom4 = 31;
                return nom4;

            }
            else if (manop4 == Mano.DoblePar)
            {
                nom4 = 32;
                return nom4;

            }
            else if (manop4 == Mano.Trio)
            {
                nom4 = 33;
                return nom4;

            }
            else if (manop4 == Mano.Escalera)
            {
                nom4 = 34;
                return nom4;

            }
            else if (manop4 == Mano.Color)
            {
                nom4 = 35;
                return nom4;

            }
            else if (manop4 == Mano.Full)
            {
                nom4 = 36;
                return nom4;

            }

            if (manop4 == Mano.Poker)
            {
                nom4 = 37;
                return nom4;

            }
            if (manop4 == Mano.EscaleraColor)
            {
                nom4 = 38;
                return nom4;

            }
            if (manop4 == Mano.EscaleraReal)
            {
                nom4 = 39;
                return nom4;

            }
            return nom4;

        }

    }
}
