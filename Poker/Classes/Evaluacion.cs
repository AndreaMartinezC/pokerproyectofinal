﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker.Classes
{
    public enum Mano { Nada, UnPar, DoblePar, Trio, Escalera, Color, Full, Poker, EscaleraColor, EscaleraReal}
    public struct HandValue
    {
        public int Total { get; set; }
        public int CartaMayor { get; set; }
    }


    class Evaluacion:Carta
    {
        private int SumaCorazon = 0;
        private int SumaEspada = 0;
        private int SumaDiamante = 0;
        private int SumaTrebol = 0;
        private Carta[] cartas;
      
        private HandValue valormano; 

        public Evaluacion()
        {
            SumaCorazon = 0;
            SumaDiamante = 0;
            SumaEspada = 0;
            SumaTrebol = 0;
            //cartasv = ManoOrd;
            cartas = new Carta[5];
            valormano = new HandValue();
        }

        public HandValue valoresmano
        {
            get { return valormano; }
            set { valormano = value; }
        }

        /*public Carta[] cartasv
        {
            get { return cartas; }
            set
            {
                cartas[0] = value[0];
                cartas[1] = value[1];
                cartas[2] = value[2];
                cartas[3] = value[3];
                cartas[4] = value[4];
            }
        }*/

        public Mano EvaluarMano(Carta[] jk)
        {
            //obtenerelnumerodelpalo
            cartas = jk;
           //ObtenerPalo(jk);
            if (EscaleraReal())
                return Mano.EscaleraReal;
            else if (EscaleraColor())
                return Mano.EscaleraColor;
            else if (ManoPoker())
                return Mano.Poker;
            else if (Full())
                return Mano.Full;
            else if (Color())
                return Mano.Color;
            else if (Escalera())
                return Mano.Escalera;
            else if (Trio())
                return Mano.Trio;
            else if (DosPares())
                return Mano.DoblePar;
            else if (Par())
                return Mano.UnPar;
            else
                //valormano.CartaMayor = (int)cartas[4].valor;
                return Mano.Nada;
            




        }

        private void ObtenerPalo(Carta[] ki)
        {
            foreach(var elemento in ki)
            {
                if (elemento.palo == Carta.PALO.Corazon)
                    SumaCorazon++;
                else if (elemento.palo == Carta.PALO.Diamante)
                    SumaDiamante++;
                else if (elemento.palo  == Carta.PALO.Espada)
                    SumaEspada++;
                else if (elemento.palo == Carta.PALO.Trebol )
                    SumaTrebol++;
            }
        }
        

        private bool EscaleraReal()
        {
            if ((SumaCorazon == 5 || SumaDiamante == 5 || SumaEspada == 5 || SumaTrebol == 5) && cartas[0].valor == Carta.VALOR.Diez && cartas[1].valor == Carta.VALOR.Joker  && cartas[2].valor == Carta.VALOR.Reina && cartas[3].valor == Carta.VALOR.Rey && cartas[4].valor== Carta.VALOR.As)
            {
                valormano.Total = valormano.Total = (int)(cartas[0].valor) + (int)(cartas[1].valor) + (int)(cartas[2].valor) + (int)(cartas[3].valor) + (int)(cartas[4].valor);
                return true;
            }
            return false;
        }


        private bool EscaleraColor()
        {
            if ((SumaCorazon == 5 || SumaDiamante == 5 || SumaEspada == 5 || SumaTrebol == 5) && cartas[0].valor + 1 == cartas[1].valor && cartas[0].valor + 2 == cartas[2].valor && cartas[0].valor + 3 == cartas[3].valor && cartas[0].valor + 4 == cartas[4].valor)
            {
                valormano.Total = (int)cartas[4].valor;
                return true;
            }
            return false;
        }

    

        private bool ManoPoker()
        {
            //si hay 4 cartas del mismo tipo 
            if(cartas[0].valor==cartas[1].valor && cartas[0].valor == cartas[2].valor && cartas[0].valor == cartas[3].valor)
            {
                valormano.Total = (int)cartas[0].valor * 4;
                valormano.CartaMayor = (int)cartas[4].valor;
                return true;
            }
             else if (cartas[1].valor == cartas[2].valor && cartas[1].valor == cartas[3].valor && cartas[1].valor == cartas[4].valor)
            {
                valormano.Total = (int)cartas[1].valor * 4;
                valormano.CartaMayor = (int)cartas[0].valor;
                return true;
            }

            return false;
        }

        private bool Full()
        {
            if ((cartas[0].valor == cartas[1].valor && cartas[0].valor == cartas[2].valor && cartas[3].valor == cartas[4].valor) || (cartas[0].valor == cartas[1].valor && cartas[2].valor == cartas[3].valor && cartas[2].valor == cartas[4].valor))
            {
                valormano.Total = (int)(cartas[0].valor) + (int)(cartas[1].valor) + (int)(cartas[2].valor) + (int)(cartas[3].valor) + (int)(cartas[4].valor);
                return true;
            }
            return false;

      
        }

        private bool Color()
        {
            if(SumaCorazon==5 || SumaDiamante==5 || SumaEspada ==5 || SumaTrebol==5)
            {
                valormano.Total = (int)cartas[4].valor;//tomas este valor porque va a ganar el que tenga la carta de mayor valor
                return true;
            }
            return false;
        }

        private bool Escalera()
        {
            if(cartas[0].valor + 1 == cartas[1].valor && cartas[0].valor + 2 == cartas[2].valor && cartas[0].valor + 3 == cartas[3].valor && cartas[0].valor + 4 == cartas[4].valor)
            {
                valormano.Total = (int)cartas[4].valor;
                return true;

            }

            return false;
        }

        private bool Trio()
        {
            if((cartas[0].valor == cartas[1].valor && cartas[0].valor == cartas[2].valor) || (cartas[1].valor == cartas[2].valor && cartas[1].valor == cartas[3].valor))
            {
                valormano.Total = (int)cartas[2].valor * 3;
                valormano.CartaMayor = (int)cartas[4].valor;
                return true;
            }
            else if((cartas[2].valor == cartas[3].valor && cartas[2].valor == cartas[4].valor))
            {
                valormano.Total = (int)cartas[2].valor * 3;
                valormano.CartaMayor = (int)cartas[1].valor;
                return true;
            }
            return false;
        }

        private bool DosPares()
        {
            //0=1 && 2=3
            //0=1 && 3=4
            //1=2 && 3=4

            if (cartas[0].valor == cartas[1].valor && cartas[2].valor == cartas[3].valor)
            {
                valormano.Total = ((int)cartas[0].valor * 2) + ((int)cartas[2].valor * 2);
                valormano.CartaMayor = (int)cartas[4].valor;
                return true;
            }
            else if (cartas[0].valor == cartas[1].valor && cartas[3].valor == cartas[4].valor)
            {
                valormano.Total = ((int)cartas[0].valor * 2) + ((int)cartas[3].valor * 2);
                valormano.CartaMayor = (int)cartas[2].valor;
                return true;
            }
            else if (cartas[1].valor == cartas[2].valor && cartas[3].valor == cartas[4].valor)
            {
                valormano.Total = ((int)cartas[1].valor * 2) + ((int)cartas[3].valor * 2);
                valormano.CartaMayor = (int)cartas[0].valor;
                return true;
            }
            return false; 
        }

        private bool Par()
        {
            /* 
             0=1
             1=2
             2=3
             3=4  
             */

            if (cartas[0].valor == cartas[1].valor)
            {
                valormano.Total = ((int)cartas[0].valor * 2);
                valormano.CartaMayor = (int)cartas[4].valor;
                return true;
            }
            else if (cartas[1].valor == cartas[2].valor)
            {
                valormano.Total = ((int)cartas[1].valor * 2);
                valormano.CartaMayor = (int)cartas[4].valor;
                return true;
            }
            else if (cartas[2].valor == cartas[3].valor)
            {
                valormano.Total = ((int)cartas[2].valor * 2) ;
                valormano.CartaMayor = (int)cartas[4].valor;
                return true;
            }
            else if (cartas[3].valor == cartas[4].valor)
            {
                valormano.Total = ((int)cartas[3].valor * 2);
                valormano.CartaMayor = (int)cartas[2].valor;
                return true;
            }

            return false;


        }


    }
}
